import socket
import time

from threading import Thread

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

sock.setblocking(0)
sock.bind((UDP_IP, UDP_PORT))


count = -1
lastTime = time.time()

def watch():
  global count
  global lastTime

  while lastTime != -1:
    try:
      data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    except IOError:
      pass
    else:
      #print "Watching: ", data
      count = int(data)
      lastTime = time.time()

  
watcher = Thread(target = watch, args = (),)

watcher.start()
while (time.time() - lastTime) <= 1:
  time.sleep(0.1)
lastTime = -1
#print "Transmisson ended"
watcher.join()
print count