// Go 1.2
// go run helloworld_go.go

package main
//import "sync"


import (
    "fmt"
    "time"
)

//var l = &sync.Mutex{}
var i int = 0

func thread1(done chan bool) {
    for n := 0; n < 1000000; n++ {
        i++
    }
    //l.Unlock()
    done <- true
}

func thread2(done chan bool) {
    for n := 0; n < 1000000; n++ {
        i--
    }
    //l.Unlock()
    done <- true
}

func main() {
    // I guess this is a hint to what GOMAXPROCS does...
    
    //l.Lock()
    done := make(chan bool, 1)                                        // Try doing the exercise both with and without it!
    go thread1(done)
    //l.Lock()
    <- done
    go thread2(done)                      // This spawns someGoroutine() as a goroutine

    // We have no way to wait for the completion of a goroutine (without additional syncronization of some sort)
    // We'll come back to using channels in Exercise 2. For now: Sleep.
    time.Sleep(100*time.Millisecond)
    fmt.Println(i)
}