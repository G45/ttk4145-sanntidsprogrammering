# Constants

# elev_motor_direction
DIRN_DOWN = -1
DIRN_STOP = 0
DIRN_UP = 1

# elev_lamp_type
BUTTON_CALL_UP = 0
BUTTON_CALL_DOWN = 1
BUTTON_COMMAND = 2

# Number of floors. Hardware-dependent, do not modify.
N_FLOORS = 4

# Number of buttons (and corresponding lamps) on a per-floor basis
N_BUTTONS = 3

# Number of elevators. All using the same hardware.
N_ELEV = 3

# MOTOR_SPEED
MOTOR_SPEED = 2800

# Door open time seconds
T_DOOR = 3

# Max travel time seconds
T_TRAVEL = 5

# Max network time before offline assumption
T_NETWORK = 1

# Elevator ID number {0, N_ELEV-1}
ID = 0

# Empty Queue (Not quite a constant)
emptyQueue = [[0 for x in range(N_ELEV +2)] for x in range(N_FLOORS)]