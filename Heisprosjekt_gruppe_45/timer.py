#Timer

import time
import constants as cons

class TimerClass:

	def __init__(self, timeOut):
		self.time = -1
		self.timeOut = timeOut

	def start(self):
		self.time = time.time()
		return True

	def check(self):
		return((time.time()-self.time) > self.timeOut)

	def stop(self):
		self.time = -1
		return True