import constants as cons
import network
import queue
import time
from threading import Thread
import random

N = network.NetworkClass()
Q = queue.QueueClass()

random.seed()

def sender():
	while True:
		N.sendQueue(Q.table)
		time.sleep(0.2)

def printer():
	while True:
		print Q.table
		time.sleep(0.5)

sending = Thread(target = sender, args = (),)
printing = Thread(target = printer, args = (),)
sending.start()
printing.start()

while True:
	R = random.randint(0,3)
	C = random.randint(0,4)
	val = random.randint(0,1)
	Q.update(R,C,val)
	time.sleep(0.3)