import PanelInterface as pan
import socket
import struct

P = pan.PanelInterface()

def setButtonLights():
    global P
    for r in range(4):
        P.set_button_lamp(0,r,1)
        P.set_button_lamp(1,r,1)
        P.set_button_lamp(2,r,1)

setButtonLights()
P.set_door_open_lamp(1)
P.set_stop_lamp(1)
'''
multicast_group = '224.1.1.0'
server_address = ('224.1.1.0', 10000)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(server_address)
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
	data, adress = sock.recvfrom(1024)
	print 'received ' + str(len(data)) + ' bytes from' + str(adress)
	print str(data)
'''
'''
MCAST_GRP = '224.1.1.2'
MCAST_PORT = 10002

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
sock.sendto("Through 1", (MCAST_GRP, MCAST_PORT))

MCAST_GRP = '224.1.1.1'
MCAST_PORT = 10001

sock.sendto("Through 2", (MCAST_GRP, MCAST_PORT))
'''