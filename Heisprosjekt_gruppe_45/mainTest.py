#Driver imports
import ElevInterface as elev
import PanelInterface as pan
import constants as cons

#Other module imports
import queue
import timer as T

elevInt = elev.ElevInterface()
panelInt = pan.PanelInterface()

def readButtonMatrix():
    global panelInt
    table = [[False for x in range(cons.N_BUTTONS)] for x in range(cons.N_FLOORS)]
    for r in range(cons.N_FLOORS):
        for c in range(cons.N_BUTTONS):
            table[r][c] = bool(panelInt.get_button_signal(c,r))
    return table
            

def main():
    global elevInt
    global panelInt

    print "Press STOP button to stop elevator and exit program.\n"

    elevInt.set_motor_direction(cons.DIRN_UP)

    while True:
    	currFloor = elevInt.get_floor_sensor_signal()

    	if currFloor == (cons.N_FLOORS - 1):
    		elevInt.set_motor_direction(cons.DIRN_DOWN)
    		panelInt.set_floor_indicator(currFloor)
    	elif currFloor == 0:
    		elevInt.set_motor_direction(cons.DIRN_UP)
    		panelInt.set_floor_indicator(currFloor)
    	elif currFloor > 0:
    		panelInt.set_floor_indicator(currFloor)

    	if panelInt.get_stop_signal() == 1:
    		elevInt.set_motor_direction(cons.DIRN_STOP)
    		return 0


if __name__ == "__main__":
    main()
