import constants as cons
import network
import queue
import time
from threading import Thread
import PanelInterface

N = network.NetworkClass()
Q = queue.QueueClass()
P = PanelInterface.PanelInterface()

def printQueue():
	while True:
		print Q.table
		time.sleep(0.5)

def setButtonLights(queue):
    global P
    for r in range(cons.N_FLOORS):
        P.set_button_lamp(0,r,queue[r][0])
        P.set_button_lamp(1,r,queue[r][1])
        P.set_button_lamp(cons.ID+2,r,queue[r][cons.ID+2])


printer = Thread(target = printQueue, args = (),)
printer.start()



while True:
	datatype = None
	data = N.recieve(1)
	if data != None:
		(datatype, data) = N.recParse(data)
	if datatype == 'Q':
		Q.replace(data)
	setButtonLights(Q.table)
