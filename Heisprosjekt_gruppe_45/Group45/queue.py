#Queue

import constants as cons

class QueueClass:

	def __init__(self):
		self.table = [[0 for x in range(cons.N_ELEV +2)] for x in range(cons.N_FLOORS)]
	
	
	def update(self, row, column, val):
		if((row < 0) or (column < 0)):
			print "ERROR: QueueClass.update: Negative coordinate"
		elif((row >= len(self.table)) or (column >= len(self.table[0]))):
			print "ERROR: QueueClass.update: Coordinate out of bounds"
		else:
			self.table[row][column] = val



	
	def replace(self, newTable):
		if((len(self.table) != len(newTable)) or (len(self.table[0]) != len(newTable[0]))):
			print "ERROR: QueueClass.replace: Dimensions do not agree"
		else:
			self.table = newTable

	
	def compare(self, table1, table2):
		corTable = [[0 for x in range(cons.N_ELEV + 2)] for x in range(cons.N_FLOORS)]

		if table1 is None:
			corTable = self.table

		elif table2 is None:
			for r in range(cons.N_FLOORS):
				for c in range(cons.N_ELEV+2):
					corTable[r][c] = (self.table[r][c] or table1[r][c])

		else:
			for r in range(cons.N_FLOORS):
				for c in range(cons.N_ELEV+2):
					if (self.table[r][c] + table1[r][c] + table2[r][c]) > 0:
						corTable[r][c] = 1

		return corTable, (self.table == corTable)
	
	def addOrderTable(self, table):
		for r in range(cons.N_FLOORS):
			for c in range(cons.N_ELEV+2):
				self.table[r][c] = (self.table[r][c] or table[r][c])


	def clear(self):
		self.table = cons.emptyQueue