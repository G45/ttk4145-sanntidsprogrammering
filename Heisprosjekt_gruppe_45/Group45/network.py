#Network

import constants as cons
import socket
import struct

class NetworkClass:

	def __init__(self):
		#Setup of sender
		self.sender_group = ('224.1.1.' + str(cons.ID), 10000 + cons.ID)
		self.sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sender.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
		
		#Setup of recievers
		self.reciever_adress = dict()
		self.reciever = dict()
		for n in range(cons.N_ELEV):
			if n != cons.ID:
				self.reciever_adress[n] = ('224.1.1.' + str(n), 10000 + n)
				self.reciever[n] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				self.reciever[n].bind(self.reciever_adress[n])
				mreq = struct.pack('4sL', socket.inet_aton(self.reciever_adress[n][0]), socket.INADDR_ANY)
				self.reciever[n].setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
				self.reciever[n].setblocking(0)


	def transmit(self, data):
		self.sender.sendto(data, self.sender_group)

	def masterSendOrder(self, order):
		Ostring = 'M'
		for n in range(cons.N_ELEV):
			Ostring = Ostring + str(order[n])

	def sendQueue(self, queue):
		Qstring = 'Q'
		for r in range(cons.N_FLOORS):
			for c in range(cons.N_ELEV + cons.N_BUTTONS -1):
				Qstring = Qstring + (str(queue[r][c]))
		self.transmit(Qstring)


	def sendStatus(self, status):
		Sstring = 'S'
		for x in range(3):
			Sstring = Sstring + str(status[x])
		self.transmit(Sstring)
		
	def sendCompleted(self, floor, elev):
		Cstring = 'C'
		Cstring = Cstring + str(floor) + str(cons.ID)



	def recieve(self, elev):
		try: 
			data, adress = self.reciever[elev].recvfrom(1024)
		except IOError:
			pass
		else:
			return data

	def recParse(self, data):
		datatype = data[0]
		data = data[1:]
		if datatype == 'Q':
			table = [[0 for x in range(cons.N_ELEV + cons.N_BUTTONS -1)] for x in range(cons.N_FLOORS)]
			for r in range(cons.N_FLOORS):
				for c in range(cons.N_ELEV + cons.N_BUTTONS -1):
					table[r][c] = int(data[(cons.N_ELEV + cons.N_BUTTONS -1)*r + c])
			return datatype, table

		elif datatype == 'S':
			status = [int(data[x]) for x in range(3)]
			return datatype, status

		elif datatype == 'M':
			pass

		elif datatype == 'R':
			pass

		else:
			return None

