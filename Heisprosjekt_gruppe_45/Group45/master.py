#Master

import constants as cons
import network as net


class MasterClass:

	def __init__(self):
		self.status = [[0 for x in range(3)] for x in range(cons.N_ELEV)] #3x3 matrix
		self.order = [0 for x in range(cons.N_ELEV)]
		self.FOS = -1	#Figure Of Suitability

	def update(self, newStat, elev):
		for x in range(3):
			self.status[elev][x] = newStat[x]

	def figOfSuit(self, request, elev):
		self.FOS = 0
		if self.status[elev][2] == 0:
			print "Elevator" + str(cons.ID + 1) + "is not functioning properly" 
		elif (self.status[elev][0] < request[0] and self.status[elev][1] == request[1]):
			self.FOS = ((cons.N_FLOORS + 1) - abs(self.status[elev][0] - request[0]))
		elif (self.status[elev][0] > request[0] and self.status[elev][1] == -(request[1])):
			self.FOS = ((cons.N_FLOORS + 1) - abs(self.status[elev][0] - request[0]))
		elif (self.status[elev][0] < request[0] and self.status[elev][1] != request[1]):
			self.FOS = (cons.N_FLOORS - abs(self.status[elev][0] - request[0]))
		elif (self.status[elev][0] > request[0] and self.status[elev][1] != request[1]):
			self.FOS = (cons.N_FLOORS - abs(self.status[elev][0] - request[0]))
		else:
			self.FOS = 1	


	def generateOrder(self, queue):
		for elev in range(cons.N_ELEV):
			for floor in range(cons.N_FLOORS):
				if queue[floor][elev+2] == 1: #det finnes en intern bestilling i heisen
					self.figOfSuit([floor, 0],cons.ID)
					self.order[elev] = floor
					if  self.FOS < self.figOfSuit([floor, 0],cons.ID):
						self.order[elev] = floor
						self.figOfSuit([floor, 0],cons.ID)
				if queue[floor][elev] == 1: #det finnes en ekstern bestilling i en etasje
					if self.FOS < self.figOfSuit([floor, 1],cons.ID): #oppover
						self.order[elev] = floor
						self.figOfSuit([floor, 1],cons.ID)
					if self.FOS < self.figOfSuit([floor, -1],cons.ID): #nedover
						self.order[elev] = floor
						self.figOfSuit([floor, -1],cons.ID)