#Main
#Python imports
from threading import Thread

#Constants imports
import constants as cons
import time

#Driver imports
import ElevInterface as elev
import PanelInterface as pan

#Other module imports
import queue
import timer
import master
import network

E = elev.ElevInterface()                #Elevator
P = pan.PanelInterface()                #Panel
Q = queue.QueueClass()                  #Queue
TT = timer.TimerClass(cons.T_TRAVEL)    #TravelTimer
TD = timer.TimerClass(cons.T_DOOR)      #DoorTimer
M = master.MasterClass()                #Master
N = network.NetworkClass()              #Network

STOP = False
ISMASTER = 0
status = [E.get_floor_sensor_signal(), 0, 1]
prevFloor = 0
State = "State_Idle"

NetworkTimer = dict()
for n in range(cons.N_ELEV):
    if n != cons.ID:
        NetworkTimer[n] = timer.TimerClass(cons.T_NETWORK)






def readButtonMatrix():
    table = [[0 for x in range(cons.N_ELEV + 2)] for x in range(cons.N_FLOORS)]
    for r in range(cons.N_FLOORS):
        for c in range(3):
            if c == 2:
                table[r][c+cons.ID] = P.get_button_signal(c,r)
            else:
                table[r][c] = P.get_button_signal(c,r)
            time.sleep(0.01)
    table[0][1] = 0
    table[cons.N_FLOORS-1][0] = 0
    return table



def setButtonLights(queue):
    global P
    for r in range(cons.N_FLOORS):
        P.set_button_lamp(0,r,queue[r][0])
        P.set_button_lamp(1,r,queue[r][1])
        P.set_button_lamp(2,r,queue[r][cons.ID+2])



####################
##Thread functions##
####################

def Panelupdate():
    global STOP
    global prevFloor
    global status
    #global P
    #global N
    while not STOP:
    	matrix = readButtonMatrix()
        print matrix
        print cons.emptyQueue
    	if matrix != cons.emptyQueue:
    		if ISMASTER == cons.ID:
    			Q.addOrderTable(matrix)
    		else:
    			N.sendQueue(matrix)
                print 'SendingQueue'
        if E.get_floor_sensor_signal() != -1:
            prevFloor = E.get_floor_sensor_signal()
            print prevFloor
        P.set_floor_indicator(prevFloor)
        M.status[cons.ID][0] = prevFloor
        setButtonLights(Q.table)
        STOP = bool(P.get_stop_signal())
        P.set_stop_lamp(STOP)
        time.sleep(0.1)


def Networkupdate():
    global N
    global Orderlist
    while not STOP:
        for n in range(cons.N_ELEV):
            if n != cons.ID: 
                data = N.recieve(n)
                if data: 
                    NetworkTimer[n].start()
                    datatype, data = recParse(data)
                    if datatype == 'Q':
                        if ISMASTER == elev.ID or n == ISMASTER:
                            Q.addOrderTable(data)
                    elif datatype == 'S':
                        M.update(data,n)
                    elif datatype == 'C':
                        if ISMASTER == cons.ID:
                            Q.update(int(data[0]),0,0)
                            Q.update(int(data[0]),1,0)
                            Q.update(int(data[0]),int(data[1]) +2,0)
                    elif datatype == 'M':
                        order = [0 for x in range(cons.N_ELEV)]
                        for n in range(cons.N_ELEV):
                            order[n] = int(data[n])

                elif NetworkTimer[n].check():
                    M.update([None, None, 0],n)


def Heartbeat():
    while not STOP:
        M.update(status,cons.ID)
        if ISMASTER == cons.ID:
            N.sendQueue(Q.table)
        N.sendStatus(status)
        time.sleep(0.1)



def main():
    global E
    global Q
    global M
    global State
    global ISMASTER

    
    #E.set_motor_direction(cons.DIRN_UP)
    panel = Thread(target = Panelupdate, args = (),)
    networker = Thread(target = Networkupdate, args = (),)

    currFloor = E.get_floor_sensor_signal()

    panel.start()
    networker.start()

    for n in range(cons.N_ELEV):
        if n != cons.ID:
            NetworkTimer[n].start()



    while not STOP:
        M.status[cons.ID][2] = 1
        for n in range(cons.N_ELEV):
            if M.status[n][2]:
                ISMASTER = n
                break

        if ISMASTER == cons.ID:
            M.generateOrder(Q.table)


        if State == "State_Idle":
            time.sleep(0.1)
            direction = M.order[cons.ID] - status[0]
            direction = max(min(1, direction), -1)
            if direction:
                E.set_motor_direction(direction)
                status[1] = direction
                State = "State_Running"
                TT.start()

        if State == "State_Running":
            prevfloor = currFloor
            currFloor = E.get_floor_sensor_signal()
            if currFloor == M.order[cons.ID]:
                TT.stop()
                E.set_motor_direction(0)
                status[1] = 0
                P.set_door_open_lamp(1)
                State = "State_Door_Open"
                TD.start()
            elif TT.check:
                status[2] = 0
            elif prevfloor != currfloor:
                TT.reset()


        if State == "State_Door_Open":
            if TD.check():
                TD.stop()
                P.set_door_open_lamp(0)
                if ISMASTER == cons.ID:
                    Q.update(prevFloor,0,0)
                    Q.update(prevFloor,1,0)
                    Q.update(prevFloor,cons.ID +2,0)
                else:
                    N.sendCompleted(E.get_floor_sensor_signal())
                State = "State_Idle"


        
    E.set_motor_direction(0)
    Q.clear()
    print 'program stopped'
    panel.join()
    print 'Panel stopped'
    networker.join()
    print 'Network stopped'






#'''Self start of main

if __name__ == "__main__":
    main()

#'''