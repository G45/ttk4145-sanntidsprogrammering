# Python 3.3.3 and 2.7.6
# python helloworld_python.py

from threading import Thread
from threading import Lock

i = 0
lock = Lock()

def thread1():
    lock.acquire()
    global i
    for n in range(1000000):
        #lock.acquire()
        i+=1
    lock.release()

def thread2():
    lock.acquire()
    global i
    for m in range(1000000):
        #lock.acquire()
        i-=1
    lock.release()
# Potentially useful thing:
#   In Python you "import" a global variable, instead of "export"ing it when you declare it
#   (This is probably an effort to make you feel bad about typing the word "global")
#global i


def main():

    oneThread = Thread(target = thread1, args = (),)
    anotherThread = Thread(target = thread2, args = (),)

    oneThread.start()
    anotherThread.start()
    
    oneThread.join()
    anotherThread.join()

    print(i)


main()