import socket
import time
import subprocess

from threading import Thread

UDP_IP = "127.0.0.1"
SEND_PORT = 5005
RECV_PORT = 5010

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

sock.setblocking(0)
sock.bind((UDP_IP, RECV_PORT))


count = -1
lastTime = time.time()

def watch():
  global count
  global lastTime

  while lastTime != -1:
    try:
      data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    except IOError:
      pass
    else:
      count = int(data)
      #print count
      lastTime = time.time()

def broadcast():
  global count

  while True:
    count += 1
    print count
    for x in range (0, 4):
      sock.sendto(str(count), (UDP_IP, SEND_PORT))
      time.sleep(0.25)
    

watcher = Thread(target = watch, args = (),)
transmitter = Thread(target = broadcast, args = (),)

watcher.start()
print 'Monitoring parent'
while (time.time() - lastTime) <= 1:
  time.sleep(0.1)
lastTime = -1
watcher.join()

print 'Lost parent'
subprocess.call(["gnome-terminal", "-x", "python", "O6B.py"])
print 'Backup created'
transmitter.start()