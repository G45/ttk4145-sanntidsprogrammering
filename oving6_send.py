import socket
import time

UDP_IP = "127.0.0.1"
UDP_PORT = 5010


print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT


sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP


count = 0

while True:
  print count
  for x in range (0, 4):
    sock.sendto(str(count), (UDP_IP, UDP_PORT))
    time.sleep(0.25)
  count += 1

